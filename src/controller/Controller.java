package controller;

import model.Person;
import model.Student;
import model.Teacher;
import view.View;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * {@code Class Controller} defines methods to manipulate people.
 *
 * @author ciccio
 */
public class Controller {
    private ArrayList<Person> persons;
    private View view;

    /**
     * Create a new Controller object and initialize the people list to size 20.
     */
    public Controller() {
        this.persons = new ArrayList<>(20);
    }

    /**
     * Returns the people list.
     * @return An {@code Arraylist<Person>} object.
     */
    public ArrayList<Person> getPersons() {
        return persons;
    }

    /**
     * Sets the people list.
     * @param persons An {@code Arraylist<Person>} object.
     */
    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }


    /**
     * The entrance to application.
     * Runs the program while {@code userInput} is different to exit.
     */
    public void run() {
        String userInput;
        Scanner sc = new Scanner(System.in);


        while (!(userInput = View.showMenu()).equalsIgnoreCase("exit")) {
            switch (userInput) {
                case "add student" ->this.addStudent(View.addStudent());
                case "add teacher" -> this.addTeacher(View.addTeacher());
                case "list people" -> View.listPeople(this.getPersons());
                default -> System.out.println(View.showError(userInput));
            }
        }

        sc.close();
    }

    /**
     * Add a student to the people list.
     *
     * @param student The student object.
     */
    public void addStudent(String student) {
        String[] userArray = student.split(" ");

        this.persons.add(new Student(userArray[0], userArray[1], Integer.parseInt(userArray[2]),
                Integer.parseInt(userArray[3]), Integer.parseInt(userArray[4])));

    }

    /**
     * Add a teacher to the people list.
     *
     * @param teacher The teacher object.
     */
    public void addTeacher(String teacher) {
        String[] teacherArray = teacher.split(" ");

        this.persons.add(new Teacher(teacherArray[0], teacherArray[1],
                Integer.parseInt(teacherArray[2]), teacherArray[3],
                Integer.parseInt(teacherArray[4])));

    }


    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.run();
    }
}
