package view;

import controller.Controller;
import model.Person;
import model.Student;
import model.Teacher;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * {@code Class View} defines a static methods to enhance the user experience.
 *
 * @author ciccio
 */
public class View {

    /**
     * The menu options.
     */
    private final static String menuOptions = """
            Please enter one of the following options:
                add student
                add teacher
                list people
                exit""";

    /**
     * Prints out the menu and returns the user input.
     *
     * @return An object string.
     */
    public static String showMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println(menuOptions);
        return sc.nextLine();
    }


    /**
     * Prints out a menu to add a student and returns the user input.
     *
     * @return A string object.
     */
    public static String addStudent() {
        Scanner sc = new Scanner(System.in);


        System.out.println("""
                            Please enter the following items for the new student, all on the same line:
                            LastName FirstName Age StudentID GradeLevel""");

        return sc.nextLine();
    }

    /**
     * Prints out a menu to add a teacher and returns the user input.
     *
     * @return A string object.
     */
    public static String addTeacher() {
        Scanner sc = new Scanner(System.in);


        System.out.println("""
                            Please enter the following items for the new teacher, all on the same line:
                            LastName FirstName Age Classroom Salary""");

        return sc.nextLine();
    }

    /**
     * Prints the people list.
     *
     * @param persons The people list.
     */
    public static void listPeople(ArrayList<Person> persons) {
        System.out.println("The school contains the following people:");
        persons.forEach(person -> {
            if (person instanceof Teacher teacher) {
                System.out.printf("%d) %s %s: %d, classroom = %s, salary = %d%n",
                        persons.indexOf(teacher), teacher.getLastName(),
                        teacher.getFirstName(), teacher.getAge(),
                        teacher.getClassroom(), teacher.getSalary());
                System.out.println();
            } else if (person instanceof Student student) {
                System.out.printf("%d) %s %s: %d, StudentID = %s, Grade Level = %d%n",
                        persons.indexOf(student), student.getLastName(),
                        student.getFirstName(), student.getAge(),
                        student.getStudentID(), student.getGradeLevel());
            }
        });
    }

    /**
     * Returns an error based on a string param.
     * @param error The error string.
     * @return A string object.
     */
    public static String showError(String error) {
        return "%s isn't correct choice, try again.".formatted(error);
    }
}
