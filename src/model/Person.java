package model;

/**
 * {@code Class Person} defines a new Person object.
 *
 * @author ciccio
 */
public class Person {
    private String firstName;
    private String lastName;
    private int age;


    /**
     * Creates new person Object.
     *
     * @param firstName Person first name
     * @param lastName  Person last name
     * @param age       Person age
     */
    public Person(String lastName, String firstName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    /**
     * Returns person name.
     *
     * @return A string object
     * */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns person surname.
     *
     * @return A string object
     * */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns person age.
     *
     * @return An integer object
     * */
    public int getAge() {
        return age;
    }

    /**
     * Increases the age by one.
     * */
    public void happyBirthday() {
        this.age = this.age + 1;
    }

    /**
     * Returns the {@code Person} object formatted.
     *
     * @return A string object.
     * */
    @Override
    public String toString() {
        return "%s %s: %d".formatted(getFirstName(), getLastName(), getAge());
    }
}
