package model;

/**
 * {@code Class Person} defines a new Person object.
 *
 * @author ciccio
 * @see model.Person
 */
public class Student extends Person {
    private int studentID;
    private int gradeLevel;

    /**
     * Creates new student Object.
     *
     *
     * @param lastName The Student surname.
     * @param firstName The student name.
     * @param age The student age.
     * @param studentID The student ID.
     * @param gradeLevel The student grade level.
     */
    public Student(String lastName, String firstName, int age, int studentID, int gradeLevel) {
        super(lastName, firstName, age);
        this.studentID = studentID;
        this.gradeLevel = gradeLevel;
    }


    /**
     * Returns the student id.
     *
     * @return An integer object.
     */
    public int getStudentID() {
        return studentID;
    }


    /**
     * Return the garde level.
     * @return An integer object.
     */
    public int getGradeLevel() {
        return gradeLevel;
    }

    /**
     * Returns the {@code Student} object formatted.
     *
     * @return A string object.
     * */
    @Override
    public String toString() {
        return super.toString() + " ( %d - %d )".formatted(this.getStudentID(),
                this.getGradeLevel());
    }
}
