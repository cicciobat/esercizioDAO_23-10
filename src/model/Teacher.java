package model;

/**
 * {@code Class Teacher} defines a new Teacher object.
 *
 * @author ciccio
 * @see model.Person
 */
public class Teacher extends Person {
    private String classroom;
    private int salary;

    /**
     * Creates new teacher object.
     *
     * @param lastName The teacher name.
     * @param firstName The teacher surname
     * @param age The teacher age.
     * @param classroom The teacher class.
     * @param salary The teacher salary.
     */
    public Teacher(String lastName, String firstName, int age, String classroom, int salary) {
        super(lastName, firstName, age);
        this.classroom = classroom;
        this.salary = salary;
    }


    /**
     * Returns the teacher class.
     *
     * @return A string object.
     */
    public String getClassroom() {
        return classroom;
    }

    /**
     * Returns the teacher salary.
     *
     * @return An integer object.
     */
    public int getSalary() {
        return salary;
    }


    /**
     * Returns the {@code Teacher} object formatted.
     *
     * @return A string object.
     * */
    @Override
    public String toString() {
        return super.toString() + " ( %s - %d )".formatted(this.getClassroom(), this.getSalary());
    }
}
